
import 'package:TrueSaints/pages/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;
import 'package:google_sign_in/google_sign_in.dart';
import 'pages/profile_page.dart';

void main() => runApp(MyApp());

// class MyApp extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() => _MyAppState();
// }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        fontFamily: 'Raleway'
      ),
     // home: ProfilePage()
     home: _MyAppState(),
    );
  }
}

class _MyAppState extends StatelessWidget {
  bool _isLoggedIn = false;
  Map userProfile;
  final facebookLogin = FacebookLogin();
  GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);

  _loginWithFB(context) async {
    final result = await facebookLogin.logInWithReadPermissions(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get(
            'https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=${token}');
        final profile = JSON.jsonDecode(graphResponse.body);
        print(profile);
        //setState(() {
          userProfile = profile;
          _isLoggedIn = true;
            Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => new ProfilePage())
                  );
  
       // });
        break;

      case FacebookLoginStatus.cancelledByUser:
        // setState(() => _isLoggedIn = false);
        _isLoggedIn = false;
        break;
      case FacebookLoginStatus.error:
        // setState(() => _isLoggedIn = false);
        _isLoggedIn = false;
        break;
    }
  }

  _loginWithGoogle(context) async {
    try {
      await _googleSignIn.signIn();
      // setState(() {
        _isLoggedIn = true;
          Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => new ProfilePage())
         );
      // });
    } catch (err) {
      print(err);
    }
  }

  _logout() {
    _googleSignIn.signOut();
    facebookLogin.logOut();
    // setState(() {
      _isLoggedIn = false;
    // });
  }

  @override
  Widget build(BuildContext context) {
    // return MaterialApp(
      return Scaffold(
        body: Center(
          //child: _isLoggedIn
          // ? Column(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     children: <Widget>[
          //       Image.network(userProfile["picture"]["data"]["url"], height: 50.0, width: 50.0,),
          //       Text(userProfile["name"]),
          //       OutlineButton( child: Text("Logout"), onPressed: (){
          //         _logout();
          //       },)
          //     ],
          //   )
          // : Center(
          //     child: OutlineButton(
          //       child: Text("Login with Facebook"),
          //       onPressed: () {
          //         _loginWithFB();
          //       },
          //     ),
          //   ),

          // ? Column(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     children: <Widget>[
          //       Image.network(_googleSignIn.currentUser.photoUrl, height: 50.0, width: 50.0,),
          //       Text(_googleSignIn.currentUser.displayName),
          //       OutlineButton( child: Text("Logout"), onPressed: (){
          //         _logout();
          //       },)
          //     ],
          //   )
          // : Center(
          //     child: OutlineButton(
          //       child: Text("Login with Google"),
          //       onPressed: () {
          //         _loginWithGoogle();
          //       },
          //     ),
          //   )

          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  _loginWithFB(context);
                  
                },
                child: const Text('Login With Facebook',
                    style: TextStyle(fontSize: 20)),
              ),
              const SizedBox(height: 30),
              RaisedButton(
                onPressed: () {
                  _loginWithGoogle(context);
                },
                textColor: Colors.white,
                padding: const EdgeInsets.all(0.0),
                child: Container(
                  padding: const EdgeInsets.all(10.0),
                  child: const Text('Login With Google',
                      style: TextStyle(fontSize: 20)),
                ),
              ),
            ],
          ),
        ),
      );
    // );
  }
}
